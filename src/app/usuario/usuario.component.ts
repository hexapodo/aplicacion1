import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  usuarios: any = [];

  constructor(
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.usuarios = this.usuarioService.getAll().subscribe(
      (data) => {
        this.usuarios = data;
      },
      (error) => {
        console.error('este es un error: ', error);
      }
    );
  }



  }
