import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UsuarioService {

    url = 'http://localhost:3000/usuarios';

    constructor(
        private http: HttpClient
    ) { }

    getAll() {
        return this.http
            .get(this.url)
            .pipe(
                map((data) => {
                    return data;
                })
            );
    }
}
