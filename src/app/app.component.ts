import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    operacion = 'texto inicial';

    miMenu = [
        {
            key: 'test_1',
            label: 'test 1',
            link: '/usuarios'
        },
        {
            key: 'test_2',
            label: 'test 2',
            link: '/admin'
        },
        {
            key: 'test_3',
            label: 'test 3',
            link: '#'
        },
        {
            key: 'test_4',
            label: 'test 4',
            link: '#'
        },
        {
            key: 'test_5',
            label: 'otro boton',
            link: '#'
        }
    ];

    constructor() {}

    ngOnInit() {
    }

    botonPresionado(item) {
        switch (item.key) {
            case 'test_1': {
                console.log('hacer la suma');
                this.operacion = 'hacer la suma';
                break;
            }
            case 'test_2': {
                console.log('hacer la resta');
                this.operacion = 'hacer la resta';
                break;
            }
            default:
                console.log('no haga nada');
                this.operacion = 'no haga nada!!!';
        }
        console.log('se ha presionado un boton del menu', item);
    }
}
