import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    // entradas
    @Input() config = [];

    // salidas
    @Output() onclick: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    click(item) {
        this.onclick.emit(item);
    }
}
