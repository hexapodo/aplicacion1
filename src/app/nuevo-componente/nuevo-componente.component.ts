import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nuevo-componente',
  templateUrl: './nuevo-componente.component.html',
  styleUrls: ['./nuevo-componente.component.scss']
})
export class NuevoComponenteComponent implements OnInit {

  texto = 'texto inicial';

  constructor() { }

  ngOnInit() {
  }

  pulsame() {
    this.texto = 'otro texto';
  }

}
